# NativeScript Vue Oaxaca Sostenible

## Quick Start
Abre una terminal en la ruta del proyecto

Ejecuta el siguiente comando para instalar las dependencias:

```npm install``` para instalar las dependencias

```tns doctor``` para verificar que se cumpla con los requerimientos instalados

``` tns run <platform>``` puede ser--> (android, ios)

> Nota: Si no se cumple con el entorno de trabajo, te da tres opciones de instalación puedes seleccionar cualquiera o hacerlo manual.
 


## Publicacion

### Android
ejecutar: 
```
ns build android --release --key-store-path ../../KeyStoreAndroid30años --key-store-password Copeval@admin2018 --key-store-alias copeval --key-store-alias-password Copeval@admin2018
```
> Nota: KeyStoreAndroid30años es la key generada para la app se encuentra en en equipo sipo: -

#### iOS

abrir en xcode

plarforms/ios/OaxacaSostenible.xcworkspace

resolver errores  de compatibilidad de frameworks

seleccionar
    General/Frameworks y seleccionar  Ios
    <img src="https://i.stack.imgur.com/rVE10.png">
    <a href="https://stackoverflow.com/questions/58633853/issue-with-mac-catalyst-linking-in-object-file-built-for-ios-simulator">Recurso encontrado</a>

####errores:
 
Showing Recent Messages
Command PhaseScriptExecution failed with a nonzero exit code

solution: 
https://www.youtube.com/watch?v=L1OY9WKfQbQ

en >build settings/Architectures/release any ios sdk quitar arm64

1-Una vez configurado en xcode ir al menu `Product/Archive`
Permitir que compile, y solucionar los warnings que dependan de nosotros,

2-Al terminar aparecerá en pantalla con el titulo `Organizer` (siempre y cuando no existan errores) con un listado de las apps compiladas, seleccionar `Dristibuite App` y dar en siguiente seleccionar `App Store Connect` Seleccionar `Upload` 
esperar a que termine de subir en caso de error solucionar problemas y comenzas de nuevo desde `Archive/Product`



### Architecture
There is a single blank component located in:
- `/app/components/Home.vue` - sets up an empty page layout.

**Home** page has the following components:
- `ActionBar` - It holds the title of the page.
- `GridLayout` - The main page layout that should contains all the page content.

## Get Help
The NativeScript framework has a vibrant community that can help when you run into problems.

Try [joining the NativeScript community Slack](https://www.nativescript.org/slack-invitation-form). The Slack channel is a great place to get help troubleshooting problems, as well as connect with other NativeScript developers.

If you have found an issue with this template, please report the problem in the [NativeScript repository](https://github.com/NativeScript/NativeScript/issues).

## Contributing

We love PRs, and accept them gladly. Feel free to propose changes and new ideas. We will review and discuss, so that they can be accepted and better integrated.
