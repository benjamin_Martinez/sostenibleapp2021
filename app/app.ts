import Vue from 'nativescript-vue';



import RadChart from 'nativescript-ui-chart/vue';
import RadDataForm from 'nativescript-ui-dataform/vue';
import RadAutoComplete from 'nativescript-ui-autocomplete/vue';
import { TNSFontIcon, fonticon } from 'nativescript-fonticon';
import Home from './components/Home.vue';

import Vuex from 'vuex';

const connectivityModule = require("tns-core-modules/connectivity");
const myConnectionType = connectivityModule.getConnectionType();

//Initialize FontAwesome
TNSFontIcon.debug = false;
TNSFontIcon.paths = {'fa': './fonts/fa5-all.css'};
TNSFontIcon.loadCss();
Vue.filter('fonticon', fonticon);


import RadListView from 'nativescript-ui-listview/vue';
Vue.use(RadListView);

Vue.use(RadChart);
Vue.use(RadDataForm);
Vue.use(RadAutoComplete);
Vue.use(Vuex);

import store from './store/store.js';

import { CardView } from '@nstudio/nativescript-cardview';
Vue.registerElement('CardView', () => CardView);

Vue.registerElement('RadSideDrawer', () => require('nativescript-ui-sidedrawer').RadSideDrawer);
//Vue.registerElement('MapView', () => require('nativescript-google-maps-sdk').MapView)
Vue.registerElement("Mapbox", () => require("@nativescript-community/ui-mapbox").MapboxView)

Vue.config.silent = false;

switch (myConnectionType) {
  case connectivityModule.connectionType.none:
    // Denotes no Internet connection.
    console.log("No connection");
    break;
  case connectivityModule.connectionType.wifi:
    // Denotes a WiFi connection.
    console.log("WiFi connection");
    break;
  case connectivityModule.connectionType.mobile:
    // Denotes a mobile connection, i.e. cellular network or WAN.
    console.log("Mobile connection");
    break;
  case connectivityModule.connectionType.ethernet:
    // Denotes a ethernet connection.
    console.log("Ethernet connection");
    break;
  default:
    break;
}

Vue.registerElement(
    'CheckBox',
    () => require('@nstudio/nativescript-checkbox').CheckBox,
    {
      model: {
        prop: 'checked',
        event: 'checkedChange'
      }
    }
);

var {Application} = require('@nativescript/core');
const {device, screen} = require('@nativescript/core/platform');


if (screen.mainScreen.widthDIPs < 820 && store.state.isIOS || screen.mainScreen.widthDIPs < 740 && store.state.isAndroid){
  Application.setCssFileName('app_phone.css');

}else{
  Application.setCssFileName('app_tablet.css');
}
Application.setCssFileName('app_styles.css');



const vm=new Vue({
  render: h => h('frame', [h(Home)]),
  store,
  created(){
    this.$store.dispatch('getProgsAndDeps');
  }
}).$start()
