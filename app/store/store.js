import Vue from 'vue';
import Vuex from 'vuex';
import * as http from "@nativescript/core/http";
import { isAndroid, isIOS, screen} from "@nativescript/core/platform";
import distritos from "../data/distritos.json";
import regiones from "../data/regiones.json";
import municipios from "../data/municipios.json";


Vue.use(Vuex);

export default  new Vuex.Store({
  state: {
    programas: [],
    dependencias:[],
    distritos,
    regiones,
    municipios,
    isAndroid: isAndroid,
    isIOS: isIOS,
    widthDIPs: screen.width,
    heightDIPs: screen.height,
  },
  mutations:{
    loadProgramas(store,programas){
      Object.assign(store.programas, programas)
    },
    loadDependencias(store,dependencias){
      Object.assign(store.dependencias, dependencias)
    }
  },
  actions:{
    getProgsAndDeps(context){
      return new Promise(( res, rej ) => {
        http.getJSON("http://sgps.oaxaca.gob.mx/publico/loadProgrmasDependencias")
            .then(result => {
              var programas= result.filter(programa => programa.tipo=="Programa");
              var dependencias= result.filter(dependencia => dependencia.tipo=="Dependencia");
              // console.log("dependencias------->"+JSON.stringify(dependencias));
              context.commit("loadProgramas", programas);
              context.commit("loadDependencias",  dependencias);
              res();
            })
            .catch(() => rej());
      });
    },
  }
});